const CrawlE = require('crawl-e/v0.4.9')

let crawlE = new CrawlE({
  cinemas: [{
    name: 'DAS KINO',
    address: 'Giselakai 11, A-5020 Salzburg',
    website: 'http://www.daskino.at/',
    phone: '0662-87 31 00'
  }]
})
crawlE.crawl()